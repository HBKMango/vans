package app.android.vansallende.presenters

import android.content.Context
import android.graphics.Bitmap
import android.os.Handler
import android.os.Looper
import app.android.vansallende.model.ModelAdd
import app.android.vansallende.model.NewShip
import app.android.vansallende.presenters.contracts.ContractAdd

class PresenterAdd(private var view: ContractAdd.View?, context : Context) :
    ContractAdd.Presenter, ContractAdd.Model.OnFinishedListener{

    private var model: ContractAdd.Model = ModelAdd(context)

    //TODO --------------------------------------Presenter------------------------------------------

    override fun onDestroy() {
        view = null
    }

    override fun addShip(data: NewShip) {
        view?.showLottie()
        model.addShip(this, data)
    }
    //TODO ----------------------------------------Finish-------------------------------------------

    override fun onShipAdded(data: String) {
        //view?.hideLottie()
        //view?.onShipAdded(data)
        Handler(Looper.getMainLooper()).postDelayed(
            {
                view?.hideLottie()
                view?.onShipAdded(data)
        }, 1500)
    }

    override fun onFailure(throwable: String) {
        //view?.hideLottie()
        //view?.onResponseFailure(throwable)
        Handler(Looper.getMainLooper()).postDelayed(
            {
                view?.hideLottie()
                view?.onResponseFailure(throwable)
            }, 1000)
    }
}