package app.android.vansallende.presenters

import app.android.vansallende.model.ModelShipping
import app.android.vansallende.model.NewShip
import app.android.vansallende.presenters.contracts.ContractShipping

class PresenterShipping(private var view: ContractShipping.View?)
    : ContractShipping.Presenter, ContractShipping.Model.OnFinishedListener {

    private var model: ContractShipping.Model = ModelShipping()

    //TODO --------------------------------------Presenter------------------------------------------
    override fun onDestroy() {
        view = null
    }

    override fun getShipping() {
        model.getShipping(this)
    }

    //TODO ----------------------------------------Finish-------------------------------------------
    override fun onShippingGotten(data: List<NewShip>) {
        view?.onShippingGotten(data)
    }

    override fun onFailure(throwable: String) {
        view?.onResponseFailure(throwable)
    }
}