package app.android.vansallende.presenters.contracts

import app.android.vansallende.model.NewShip

interface ContractAdd {
    interface Model {
        interface OnFinishedListener {
            fun onShipAdded(data : String)
            fun onFailure(throwable: String)
        }
        fun addShip(onFinishedListener: OnFinishedListener, data : NewShip)
    }

    interface View {
        fun onShipAdded(data : String)
        fun onResponseFailure(throwable: String)
        fun showLottie()
        fun hideLottie()
    }

    interface Presenter {
        fun onDestroy()
        fun addShip(data : NewShip)
    }
}