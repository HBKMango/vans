package app.android.vansallende.presenters.contracts

import app.android.vansallende.model.NewShip

interface ContractPreview {

    interface Model {
        interface OnFinishedListener {
            fun onFound(data : NewShip?)
            fun onFailure(throwable: String)
        }
        fun browseShip(onFinishedListener: OnFinishedListener, data : String)
    }

    interface View {
        fun onFound(data : NewShip?)
        fun onResponseFailure(throwable: String)
        fun showLottie()
        fun hideLottie()
    }

    interface Presenter {
        fun onDestroy()
        fun browseShip(id : String)
    }

}