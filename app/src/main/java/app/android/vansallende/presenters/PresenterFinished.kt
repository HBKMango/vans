package app.android.vansallende.presenters

import app.android.vansallende.model.ModelFinished
import app.android.vansallende.model.NewShip
import app.android.vansallende.presenters.contracts.ContractFinished

class PresenterFinished(private var view: ContractFinished.View?) :
    ContractFinished.Presenter, ContractFinished.Model.OnFinishedListener {

    private var model: ContractFinished.Model = ModelFinished()

    //TODO --------------------------------------Presenter------------------------------------------

    override fun onDestroy() {
        view = null
    }

    override fun getShipping() {
        model.getShipping(this)
    }

    //TODO ----------------------------------------Finish-------------------------------------------

    override fun onShippingGotten(data: List<NewShip>) {
        view?.onShippingGotten(data)
    }

    override fun onFailure(throwable: String) {
        view?.onResponseFailure(throwable)
    }
}