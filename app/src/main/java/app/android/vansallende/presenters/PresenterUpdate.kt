package app.android.vansallende.presenters

import android.content.Context
import android.graphics.Bitmap
import app.android.vansallende.model.ModelUpdate
import app.android.vansallende.model.NewShip
import app.android.vansallende.presenters.contracts.ContractUpdate

class PresenterUpdate(private var view: ContractUpdate.View?, context : Context) :
    ContractUpdate.Presenter, ContractUpdate.Model.OnFinishedListener{

    private var model: ContractUpdate.Model = ModelUpdate(context)

    //TODO --------------------------------------Presenter------------------------------------------

    override fun onDestroy() {
        view = null
    }

    override fun browseShip(id: String) {
        model.browseShip(this, id)
    }

    override fun updateShip(state: String) {
        model.updateShip(this, state)
    }

    override fun finishShip(state: String, bitmap: Bitmap) {
        model.finishShip(this, state, bitmap)
    }

    //TODO ----------------------------------------Finish-------------------------------------------

    override fun onFound(data: NewShip?) {
        view?.onFound(data)
    }

    override fun onUpdated() {
        view?.onUpdated()
    }

    override fun onFinished() {
        view?.onFinished()
    }

    override fun onFailure(throwable: String) {
        view?.onResponseFailure(throwable)
    }
}