package app.android.vansallende.presenters.contracts

import android.graphics.Bitmap
import app.android.vansallende.model.NewShip

interface ContractUpdate {

    interface Model {
        interface OnFinishedListener {
            fun onFound(data : NewShip?)
            fun onUpdated()
            fun onFinished()
            fun onFailure(throwable: String)
        }
        fun browseShip(onFinishedListener: OnFinishedListener, data : String)
        fun updateShip(onFinishedListener: OnFinishedListener, data : String)
        fun finishShip(onFinishedListener: OnFinishedListener, data : String, bitmap: Bitmap)
    }

    interface View {
        fun onFound(data : NewShip?)
        fun onUpdated()
        fun onFinished()
        fun onResponseFailure(throwable: String)
        fun showLottie()
        fun hideLottie()
    }

    interface Presenter {
        fun onDestroy()
        fun browseShip(id : String)
        fun updateShip(state : String)
        fun finishShip(state : String, bitmap: Bitmap)
    }

}