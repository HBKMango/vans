package app.android.vansallende.presenters.contracts

import app.android.vansallende.model.NewShip

interface ContractShipping {
    interface Model {
        interface OnFinishedListener {
            fun onShippingGotten(data : List<NewShip>)
            fun onFailure(throwable: String)
        }
        fun getShipping(onFinishedListener: OnFinishedListener)
    }

    interface View {
        fun onShippingGotten(data : List<NewShip>)
        fun onResponseFailure(throwable: String)
        fun showLottie()
        fun hideLottie()
    }

    interface Presenter {
        fun onDestroy()
        fun getShipping()
    }
}