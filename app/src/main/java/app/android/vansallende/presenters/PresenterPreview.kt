package app.android.vansallende.presenters

import android.content.Context
import app.android.vansallende.model.ModelPreview
import app.android.vansallende.model.NewShip
import app.android.vansallende.presenters.contracts.ContractPreview

class PresenterPreview(private var view: ContractPreview.View?, context : Context)
    : ContractPreview.Presenter, ContractPreview.Model.OnFinishedListener {

    private var model: ContractPreview.Model = ModelPreview(context)

    //TODO --------------------------------------Presenter------------------------------------------

    override fun onDestroy() {
        view = null
    }

    override fun browseShip(id: String) {
        model.browseShip(this, id)
    }

    //TODO ----------------------------------------Finish-------------------------------------------

    override fun onFound(data: NewShip?) {
        view?.onFound(data)
    }

    override fun onFailure(throwable: String) {
        view?.onResponseFailure(throwable)
    }
}