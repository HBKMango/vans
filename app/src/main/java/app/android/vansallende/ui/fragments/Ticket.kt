package app.android.vansallende.ui.fragments

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.android.vansallende.R
import app.android.vansallende.databinding.FragmentTicketBinding
import app.android.vansallende.ui.util.ShareImage
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment
import java.io.File

class Ticket(val file : File) : SupportBlurDialogFragment() {

    private lateinit var mBinding : FragmentTicketBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
        isCancelable = false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_ticket, container, false)

        mBinding = FragmentTicketBinding.bind(view)

        return view
    }

    override fun onStart() {
        super.onStart()

        val dialog = dialog
        if (dialog != null){
            dialog.window!!.setLayout(
                resources.displayMetrics.widthPixels * 1,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }

        mBinding.closeView.setOnClickListener {
            dialog!!.dismiss()
        }

        mBinding.imgTicket.setImageURI(Uri.fromFile(file))

        mBinding.send.setOnClickListener {
            ShareImage.sendImage(file, context!!)
        }
    }
}