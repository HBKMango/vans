package app.android.vansallende.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import app.android.vansallende.R
import app.android.vansallende.ui.adapters.AdapterImage
import app.android.vansallende.ui.adapters.TicketListener
import app.android.vansallende.ui.util.ShareImage
import kotlinx.android.synthetic.main.fragment_tickets_list.*
import java.io.File

class TicketsList : Fragment(), TicketListener {

    private lateinit var adapter: AdapterImage
    private lateinit var sendDialog: Ticket

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tickets_list, container, false)
    }

    override fun onStart() {
        super.onStart()

        val folder = File(context!!.getExternalFilesDir(null)!!.absolutePath + File.separator + "Tickets")
        if (!folder.exists()) {
            folder.mkdirs()
        }

        val gpath : String = context!!.getExternalFilesDir(null)!!.absolutePath + File.separator
        val spath = "Tickets"
        val fullpath = File(gpath + File.separator + spath)

        adapter = AdapterImage()
        adapter.listener = this
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = adapter

        val mLayoutManager = GridLayoutManager(context, 3)
        recycler.layoutManager = mLayoutManager

        imageReaderNew(fullpath)
    }

    private fun imageReaderNew(root: File) {
        val fileList: ArrayList<File> = ArrayList()
        val listAllFiles = root.listFiles()

        if (listAllFiles != null && listAllFiles.isNotEmpty()) {
            for (currentFile in listAllFiles) {
                if (currentFile.name.endsWith(".jpg")) {
                    fileList.add(currentFile.absoluteFile)
                }
            }
        }

        adapter.swapData(fileList)
    }

    override fun onTicket(position: Int, ticket: File) {
        sendDialog = Ticket(ticket)
        sendDialog.show(childFragmentManager, "SendTicket")
    }
}