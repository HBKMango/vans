package app.android.vansallende.ui.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.media.MediaMuxer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import app.android.vansallende.R
import app.android.vansallende.model.NewShip
import app.android.vansallende.presenters.PresenterPreview
import app.android.vansallende.presenters.contracts.ContractPreview
import app.android.vansallende.ui.util.DateUtils.getDateTime
import app.android.vansallende.ui.util.States.delivered
import com.google.android.gms.common.util.DataUtils
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.koushikdutta.ion.Ion
import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment
import kotlinx.android.synthetic.main.fragment_preview.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.ZoneId
import java.util.*
import kotlin.math.roundToInt

private const val ARG_PARAM1 = "param1"

class Preview : SupportBlurDialogFragment(), ContractPreview.View {

    private var param1: String? = null
    private lateinit var presenter: PresenterPreview

    companion object {
        @JvmStatic
        fun newInstance(param1: String) = Preview().apply {
            arguments = Bundle().apply {
                putString(ARG_PARAM1, param1)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        presenter = PresenterPreview(this, context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
        isCancelable = false

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_preview, container, false)
    }

    override fun onStart() {
        super.onStart()

        val dialog = dialog
        if (dialog != null){
            dialog.window!!.setLayout(
                resources.displayMetrics.widthPixels * 1,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }

        presenter.browseShip(param1!!)

        close.setOnClickListener {
            this.dismiss()
        }

        QRImage.setImageBitmap(generateQRCode(param1!!))

        Save.setOnClickListener {
            save(loadBitmapFromView(ContentParent!!)!!)
            Toast.makeText(context!!, "Guardado correctamente", Toast.LENGTH_SHORT).show()
        }
    }

    private fun generateQRCode(text: String): Bitmap {
        val width = 800
        val height = 800
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val codeWriter = MultiFormatWriter()
        try {
            val bitMatrix = codeWriter.encode(text, BarcodeFormat.QR_CODE, width, height)
            for (x in 0 until width) {
                for (y in 0 until height) {
                    bitmap.setPixel(x, y, if (bitMatrix[x, y]) Color.BLACK else Color.WHITE)
                }
            }
        } catch (e: WriterException) {
            e.printStackTrace()
        }
        return bitmap
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    override fun onFound(data: NewShip?) {

        if (data != null){
            xDate.text = getDateTime(data.date)
            sName.text = data.transmitter!!.name
            sAddress.text = data.transmitter!!.address
            sPhone.text = data.transmitter!!.phone
            sContent.text = data.transmitter!!.description

            rName.text = data.receiver!!.name
            rPhone.text = data.receiver!!.phone
            rStreet.text = data.receiver!!.street
            rNo.text = data.receiver!!.no
            rCol.text = data.receiver!!.suburb
            rMun.text = data.receiver!!.municipality
            rCP.text = data.receiver!!.cp
            rState.text = data.receiver!!.state

            if (data.state != delivered){
                sig.visibility = View.GONE
            }
            else{
                sig.visibility = View.VISIBLE
                Ion.with(context)
                    .load(data.signature)
                    .intoImageView(firm)
            }

            /*Ion.with(context)
                .load(data.receiverSignature)
                .intoImageView(firm1)

            Ion.with(context)
                .load(data.senderSignature)
                .intoImageView(firm2)*/
        }
    }

    override fun onResponseFailure(throwable: String) {
        Toast.makeText(activity!!.applicationContext, throwable, Toast.LENGTH_SHORT).show()
    }

    override fun showLottie() {}

    override fun hideLottie() {}

    private fun loadBitmapFromView(v: View): Bitmap? {
        val b = Bitmap.createBitmap(v.width, v.height, Bitmap.Config.ARGB_8888)
        val c = Canvas(b)
        v.draw(c)
        return b
    }

    private fun save(bitmap: Bitmap) : Uri {

        val folder = File(context!!.getExternalFilesDir(null)!!.absolutePath + File.separator + "Tickets")
        if (!folder.exists()) {
            folder.mkdirs()
        }

        val destPath: String = context!!.getExternalFilesDir(null)!!.absolutePath+File.separator + "Tickets"

        val file = File(destPath, "${System.currentTimeMillis()}.jpg")

        try {
            val stream: OutputStream = FileOutputStream(file)
            val scale : Double = bitmap.width.toDouble()/ bitmap.height.toDouble()
            val newHeight = 1000/scale
            val imageScaled = Bitmap.createScaledBitmap(bitmap, 1000, newHeight.roundToInt(), false)

            imageScaled.compress(Bitmap.CompressFormat.JPEG, 100, stream)

            stream.flush()
            stream.close()
        } catch (e: IOException){
            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
        }
        return Uri.parse(file.absolutePath)
    }
}