package app.android.vansallende.ui.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.content.FileProvider
import app.android.vansallende.BuildConfig
import java.io.File
import java.util.*


object ShareImage {

    fun sendImage(file: File, context: Context){
        val share = Intent(Intent.ACTION_SEND).apply {
            type = "image/jpeg"
            putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(
                Objects.requireNonNull(context),
                BuildConfig.APPLICATION_ID + ".provider", file))
        }

        context.startActivity(Intent.createChooser(share, "Select"))
    }
}