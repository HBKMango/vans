package app.android.vansallende.ui.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.android.vansallende.model.Item
import app.android.vansallende.R
import kotlinx.android.synthetic.main.item_option.view.*

class NavigationRVAdapter(private var items: List<Item>, private var currentPos: Int) : RecyclerView.Adapter<NavigationRVAdapter.NavigationItemViewHolder>() {

    private lateinit var context: Context

    class NavigationItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NavigationItemViewHolder {
        context = parent.context
        val navItem = LayoutInflater.from(parent.context).inflate(R.layout.item_option, parent, false)
        return NavigationItemViewHolder(navItem)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: NavigationItemViewHolder, position: Int) {
        // To highlight the selected item, show different background color

        holder.itemView.icon.setImageDrawable(items[position].icon)
        holder.itemView.title.text = items[position].name

        if (position == currentPos) {
            holder.itemView.title.setTextColor(Color.WHITE)
            holder.itemView.icon.setColorFilter(Color.WHITE)
        } else {
            holder.itemView.title.setTextColor(R.color.colorAccent)
            holder.itemView.icon.setColorFilter(R.color.colorAccent)
        }
    }

}