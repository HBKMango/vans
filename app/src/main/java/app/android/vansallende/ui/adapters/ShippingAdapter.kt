package app.android.vansallende.ui.adapters

import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.android.vansallende.R
import app.android.vansallende.model.NewShip
import kotlinx.android.synthetic.main.item_ship.view.*

class ShippingAdapter : RecyclerView.Adapter<ShippingAdapter.ShippingViewHolder>(){

    private var data: List<NewShip> = ArrayList()
    lateinit var listener: ShipListener

    fun swapData(data: List<NewShip>) {
        this.data = data
        notifyDataSetChanged()
    }

    class ShippingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var listener: ShipListener? = null

        fun bind(item: NewShip, listener: ShipListener, position: Int) = with(itemView) {
            this@ShippingViewHolder.listener = listener

            tSender.text = item.transmitter!!.name
            tReciever.text = item.receiver!!.name
            tDate.text = item.date.toLong().toRelativeTime()
            tSatate.text = item.state

            setOnClickListener {
                this@ShippingViewHolder.listener?.onNoteSelected(position, item)
            }
        }

        private fun Long.toRelativeTime(): String {
            return DateUtils.getRelativeTimeSpanString(this).toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShippingViewHolder {
        return ShippingViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_ship, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ShippingViewHolder, position: Int) =
        holder.bind(data[position], listener, position)

    override fun getItemCount(): Int = data.size
}

interface ShipListener {
    fun onNoteSelected(position: Int, ship: NewShip)
}