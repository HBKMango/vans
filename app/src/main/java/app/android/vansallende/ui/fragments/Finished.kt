package app.android.vansallende.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.android.vansallende.R
import app.android.vansallende.model.NewShip
import app.android.vansallende.presenters.PresenterFinished
import app.android.vansallende.presenters.PresenterShipping
import app.android.vansallende.presenters.contracts.ContractFinished
import app.android.vansallende.presenters.contracts.ContractShipping
import app.android.vansallende.ui.adapters.ShipListener
import app.android.vansallende.ui.adapters.ShippingAdapter
import kotlinx.android.synthetic.main.fragment_finiched.*

class Finished : Fragment(), ContractFinished.View, ShipListener {

    private lateinit var presenter : PresenterFinished
    private lateinit var adapter : ShippingAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)

        presenter = PresenterFinished(this)
        adapter = ShippingAdapter()
        adapter.listener = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter.getShipping()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_finiched, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerF.adapter = adapter
        recyclerF.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
    }


    override fun onShippingGotten(data: List<NewShip>) {
        adapter.swapData(data)
    }

    override fun onResponseFailure(throwable: String) {
        Toast.makeText(context, throwable, Toast.LENGTH_SHORT).show()
    }

    override fun showLottie() { }

    override fun hideLottie() { }

    override fun onNoteSelected(position: Int, ship: NewShip) {
        Preview.newInstance(ship.id).show(childFragmentManager, "Dialog")
    }
}