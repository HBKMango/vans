package app.android.vansallende.ui.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import app.android.vansallende.R
import app.android.vansallende.model.Content
import app.android.vansallende.model.NewShip
import app.android.vansallende.model.Receiver
import app.android.vansallende.model.Transmitter
import app.android.vansallende.presenters.PresenterAdd
import app.android.vansallende.presenters.contracts.ContractAdd
import app.android.vansallende.ui.util.DateUtils.getDateTime
import app.android.vansallende.ui.util.States
import com.guardanis.sigcap.SignatureEventListener
import com.guardanis.sigcap.SignatureResponse
import com.guardanis.sigcap.dialog.SignatureDialogBuilder
import com.guardanis.sigcap.exceptions.CanceledSignatureInputException
import com.guardanis.sigcap.exceptions.NoSignatureException
import kotlinx.android.synthetic.main.fragment_add_ship.*

class AddShip : Fragment(), ContractAdd.View {

    private lateinit var presenterAdd: PresenterAdd
    private lateinit var animation : ShowLottie
    /*var bitmap1: Bitmap? = null
    var bitmap2: Bitmap? = null*/

    override fun onAttach(context: Context) {
        super.onAttach(context)

        presenterAdd = PresenterAdd(this, context)
        animation = ShowLottie()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_ship, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onStart() {
        super.onStart()

        /*toDraw1(firm1)
        toDraw2(firm2)*/

        dateTitle.text = getDateTime(System.currentTimeMillis().toString())

        calculate.setOnClickListener {
            Quantity.text = "$${calculate()}"
        }

        Confirm.setOnClickListener {
            if(validateData()){

                val a = NewShip(
                    "0",
                    Transmitter(
                        nameSend.text.toString(),
                        sendPhone.text.toString(),
                        address.text.toString(),
                        Content.text.toString()
                    ),
                    Receiver(
                        RecieverName.text.toString(),
                        RecieverPhone.text.toString(),
                        Street.text.toString(),
                        No.text.toString(),
                        Suburb.text.toString(),
                        Municipality.text.toString(),
                        CP.text.toString(),
                        State.text.toString()
                    ),
                    Content(
                        Small.text.toString().toInt(),
                        Midle.text.toString().toInt(),
                        Big.text.toString().toInt(),
                        XBig.text.toString().toInt(),
                        Quantity.text.toString().replace("$", "").toInt()
                    ),
                    "${System.currentTimeMillis()}", "",
                    States.collected
                )

                presenterAdd.addShip(a)

                /*if (bitmap1 != null && bitmap2 != null){
                    presenterAdd.addShip(a, bitmap1!!, bitmap2!!)
                }*/
            }
        }
    }

    override fun onShipAdded(data: String) {
        Preview.newInstance(data).show(childFragmentManager, "Dialog")
    }

    override fun onResponseFailure(throwable: String) {
        Toast.makeText(context!!, throwable, Toast.LENGTH_SHORT).show()
    }

    override fun showLottie() {
        animation.show(childFragmentManager, "Load")
    }

    override fun hideLottie() {
        animation.dismiss()
    }

    private fun validateData() : Boolean {
        if (nameSend.text.isNullOrEmpty() || address.text.isNullOrEmpty() || sendPhone.text.isNullOrEmpty()
            || Content.text.isNullOrEmpty() || RecieverName.text.isNullOrEmpty() || RecieverPhone.text.isNullOrEmpty()
            || Street.text.isNullOrEmpty() || No.text.isNullOrEmpty() || Suburb.text.isNullOrEmpty()
            || Municipality.text.isNullOrEmpty() || CP.text.isNullOrEmpty() || State.text.isNullOrEmpty()) {

            TILnameSend.isErrorEnabled = true
            TILaddress.isErrorEnabled = true
            TILsendPhone.isErrorEnabled = true
            TILContent.isErrorEnabled = true
            TILRecieverName.isErrorEnabled = true
            TILRecieverPhone.isErrorEnabled = true
            TILStreet.isErrorEnabled = true
            TILNo.isErrorEnabled = true
            TILSuburb.isErrorEnabled = true
            TILMunicipality.isErrorEnabled = true
            TILCP.isErrorEnabled = true
            TILState.isErrorEnabled = true

            TILnameSend.error = "Campo requerido"
            TILaddress.error = "Campo requerido"
            TILsendPhone.error = "Campo requerido"
            TILContent.error = "Campo requerido"
            TILRecieverName.error = "Campo requerido"
            TILRecieverPhone.error = "Campo requerido"
            TILStreet.error = "Campo requerido"
            TILNo.error = "Campo requerido"
            TILSuburb.error = "Campo requerido"
            TILMunicipality.error = "Campo requerido"
            TILCP.error = "Campo requerido"
            TILState.error = "Campo requerido"

            return false
        }
        else{
            TILnameSend.isErrorEnabled = false
            TILaddress.isErrorEnabled = false
            TILsendPhone.isErrorEnabled = false
            TILContent.isErrorEnabled = false
            TILRecieverName.isErrorEnabled = false
            TILRecieverPhone.isErrorEnabled = false
            TILStreet.isErrorEnabled = false
            TILNo.isErrorEnabled = false
            TILSuburb.isErrorEnabled = false
            TILMunicipality.isErrorEnabled = false
            TILCP.isErrorEnabled = false
            TILState.isErrorEnabled = false

            return true
        }
    }

    private fun calculate() : String{
        var a = 0
        a = if (Small.text.isNullOrEmpty()){ 0 } else{ Small.text.toString().toInt() }

        var b = 0
        b = if (Midle.text.isNullOrEmpty()){ 0 } else{ Midle.text.toString().toInt() }

        var c = 0
        c = if (Big.text.isNullOrEmpty()){ 0 } else{ Big.text.toString().toInt() }

        var d = 0
        d = if (XBig.text.isNullOrEmpty()){ 0 } else{ XBig.text.toString().toInt() }

        if (Small.text.isNullOrEmpty()){ Small.setText("0") }
        if (Midle.text.isNullOrEmpty()){ Midle.setText("0") }
        if (Big.text.isNullOrEmpty()){ Big.setText("0") }
        if (XBig.text.isNullOrEmpty()){ XBig.setText("0") }

        return ((90*a)+(130*b)+(170*c)+(210*d)).toString()
    }

    /*private fun toDraw1(imageView: ImageView){
        val eventListener: SignatureEventListener = object : SignatureEventListener {
            override fun onSignatureInputError(e: Throwable) {
                when (e) {
                    is NoSignatureException -> {
                        Toast.makeText(context, "Firma vacia", Toast.LENGTH_LONG).show()
                    }
                    is CanceledSignatureInputException -> {
                        // They clicked cancel
                        Toast.makeText(context, "Firma cancelada", Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        Toast.makeText(context, "Signature error", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onSignatureEntered(response: SignatureResponse) {
                val signatureImage = response.result
                bitmap1 = signatureImage
                imageView.setImageBitmap(signatureImage)
            }
        }

        imageView.setOnClickListener {
            SignatureDialogBuilder().showStatelessAlertDialog(activity, eventListener)
        }
    }

    private fun toDraw2(imageView: ImageView){
        val eventListener: SignatureEventListener = object : SignatureEventListener {
            override fun onSignatureInputError(e: Throwable) {
                when (e) {
                    is NoSignatureException -> {
                        Toast.makeText(context, "Firma vacia", Toast.LENGTH_LONG).show()
                    }
                    is CanceledSignatureInputException -> {
                        // They clicked cancel
                        Toast.makeText(context, "Firma cancelada", Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        Toast.makeText(context, "Signature error", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onSignatureEntered(response: SignatureResponse) {
                val signatureImage = response.result
                bitmap2 = signatureImage
                imageView.setImageBitmap(signatureImage)
            }
        }

        imageView.setOnClickListener {
            SignatureDialogBuilder().showStatelessAlertDialog(activity, eventListener)
        }
    }*/
}