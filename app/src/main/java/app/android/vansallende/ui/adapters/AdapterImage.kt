package app.android.vansallende.ui.adapters

import android.net.Uri
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.android.vansallende.R
import kotlinx.android.synthetic.main.item_image.view.*
import java.io.File

class AdapterImage : RecyclerView.Adapter<AdapterImage.ImageViewHolder>() {

    private var data: List<File> = ArrayList()
    lateinit var listener: TicketListener

    fun swapData(data: List<File>) {
        this.data = data
        notifyDataSetChanged()
    }

    class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private var listener: TicketListener? = null
        private var lastClickTime: Long = 0

        fun bind(item: File, listener: TicketListener, position: Int) = with(itemView) {
            this@ImageViewHolder.listener = listener

            image.setImageURI(Uri.fromFile(item))

            setOnClickListener {

                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return@setOnClickListener;
                }

                lastClickTime = SystemClock.elapsedRealtime()

                this@ImageViewHolder.listener!!.onTicket(position, item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) = holder.bind(data[position], listener, position)
}

interface TicketListener {
    fun onTicket(position: Int, ticket : File)
}