package app.android.vansallende.ui.fragments

import android.content.AbstractThreadedSyncAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.android.vansallende.R
import app.android.vansallende.UpdateActivity
import app.android.vansallende.model.NewShip
import app.android.vansallende.presenters.PresenterShipping
import app.android.vansallende.presenters.contracts.ContractShipping
import app.android.vansallende.ui.adapters.ShipListener
import app.android.vansallende.ui.adapters.ShippingAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_envios.*

class Shipping : Fragment(), ContractShipping.View, ShipListener {

    private lateinit var presenter : PresenterShipping
    private lateinit var adapter : ShippingAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        presenter = PresenterShipping(this)
        adapter = ShippingAdapter()
        adapter.listener = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.getShipping()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_envios, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
    }

    override fun onStart() {
        super.onStart()
        scanShip.setOnClickListener {
            val intent = Intent(context!!, UpdateActivity::class.java)
            startActivity(intent)
        }
    }

    // TODO --------------------------------------View----------------------------------------------

    override fun onShippingGotten(data: List<NewShip>) {
        adapter.swapData(data)
    }

    override fun onResponseFailure(throwable: String) {
        Toast.makeText(context, throwable, Toast.LENGTH_SHORT).show()
    }

    override fun showLottie() { }

    override fun hideLottie() { }

    //TODO -----------------------------------ShipSelected------------------------------------------

    override fun onNoteSelected(position: Int, ship: NewShip) {
        Preview.newInstance(ship.id).show(childFragmentManager, "Dialog")
    }
}