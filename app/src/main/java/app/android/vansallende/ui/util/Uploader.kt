package app.android.vansallende.ui.util

import android.net.Uri
import app.android.vansallende.model.FireBaseConnection
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.StorageMetadata
import com.google.firebase.storage.UploadTask
import java.io.File

object Uploader {

    fun uploadSignature(uri: Uri, fileUploadListener: FileUploadListener) {
        uploadFile(uri, "image/jpg", fileUploadListener)
    }

    fun uploadFile(uri: Uri, fileUploadListener: FileUploadListener) {
        uploadFile(uri, "audio/mp3", fileUploadListener)
    }

    private fun uploadFile(uri: Uri, type: String, fileUploadListener: FileUploadListener){

        val uploadUri = Uri.fromFile(File(uri.toString()))

        val storageRef = FireBaseConnection.initStorage().reference

        val imageRef = storageRef.child("signature/${System.currentTimeMillis()}") //${file.lastPathSegment}")
        val metadata = StorageMetadata.Builder()
                .setContentType(type)
                .build()
        val uploadTask = imageRef.putFile(uploadUri, metadata)

        uploadTask.addOnFailureListener {
            fileUploadListener.onFileUploadError(it.message)
        }.addOnSuccessListener {
            uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                return@Continuation imageRef.downloadUrl
            }).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    fileUploadListener.onFileUploaded(downloadUri.toString())
                } else {
                    fileUploadListener.onFileUploadError("Error")
                }
            }
        }
    }
}

interface FileUploadListener {
    fun onFileUploaded(url: String)
    fun onFileUploadError(error: String?)
}