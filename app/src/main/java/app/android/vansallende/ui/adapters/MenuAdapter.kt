package app.android.vansallende.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.android.vansallende.model.Item
import app.android.vansallende.R
import app.android.vansallende.ui.util.SelectableAdapter
import kotlinx.android.synthetic.main.item_option.view.*

class MenuAdapter : SelectableAdapter<MenuAdapter.MenuViewHolder>() {

    private var items: List<Item> = ArrayList()
    lateinit var listener: FragmentListener
    private val selectedPos = RecyclerView.NO_POSITION

    fun swapData(items: List<Item>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        return MenuViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_option,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        holder.itemView.isSelected = selectedPos == position
        holder.bind(items[position], listener, position,
            MenuAdapter()
        )
    }

    class MenuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private var listener: FragmentListener? = null
        private var adapter : MenuAdapter? = null

        @SuppressLint("ResourceAsColor")
        fun bind(item : Item, listener: FragmentListener, position: Int, adapter: MenuAdapter) = with(itemView) {
            this@MenuViewHolder.listener = listener
            this@MenuViewHolder.adapter = adapter

            title.text = item.name
            icon.setImageDrawable(item.icon)

            /*if (!item.isSelected){
                title.setTextColor(R.color.colorAccent)
                icon.setColorFilter(R.color.colorAccent)
            }*/

            setOnClickListener {
                this@MenuViewHolder.listener?.onFragmentSelected(position)
                //this@MenuViewHolder.adapter!!.setSelected(position)
            }
        }
    }

    fun setSelected(position : Int) {
        items.get(position).isSelected = true
        /*for (i : IndexedValue<Item> in items.withIndex()){
            if (i.index == position) {
                items[position].isSelected = true
            }
        }*/
        notifyDataSetChanged()
    }
}

interface FragmentListener{
    fun onFragmentSelected(position: Int)
}