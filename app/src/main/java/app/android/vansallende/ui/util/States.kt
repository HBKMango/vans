package app.android.vansallende.ui.util

object States {
    const val collected = "Recogido"
    const val shippingProcess = "Proceso de entrega"
    const val delivered = "Entregado"
    const val failed = "Fallido"
    const val redeliveryProcess = "Proceso de reentrega"
}

enum class State{
    Collected,
    ShippingProcess,
    Delivered,
    Failed,
    RedeliveryProcess
}