package app.android.vansallende.ui.util

import app.android.vansallende.model.Content
import app.android.vansallende.model.NewShip
import app.android.vansallende.model.Receiver
import app.android.vansallende.model.Transmitter
import com.google.firebase.firestore.DocumentSnapshot

object ModelUtils {

    fun DocumentSnapshot.toShip() : NewShip?{
        return if (this.exists()) this.toObject(NewShip::class.java) else NewShip(
            "",
            Transmitter("Error", "", "",""),
            Receiver("", "", "", "","","", "", ""),
            Content(0,0,0,0,0),
            date = "Error",
            signature = "",
            state = ""
        )
    }
}