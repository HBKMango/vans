package app.android.vansallende.ui.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import app.android.vansallende.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment

class ShowLottie : SupportBlurDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
        isCancelable = false
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_show_lottie, container, false)
    }

    override fun onStart() {
        super.onStart()

        val dialog = dialog
        if (dialog != null){
            dialog.window!!.setLayout(
                resources.displayMetrics.widthPixels * 1,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
    }
}