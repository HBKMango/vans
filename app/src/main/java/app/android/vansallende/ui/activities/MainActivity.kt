package app.android.vansallende.ui.activities

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import app.android.vansallende.R
import app.android.vansallende.ui.adapters.FragmentListener
import app.android.vansallende.ui.adapters.NavigationRVAdapter
import app.android.vansallende.model.Item
import app.android.vansallende.ui.fragments.AddShip
import app.android.vansallende.ui.fragments.Finished
import app.android.vansallende.ui.fragments.Shipping
import app.android.vansallende.ui.fragments.TicketsList
import app.android.vansallende.ui.util.ClickListener
import app.android.vansallende.ui.util.RecyclerTouchListener
import com.google.firebase.auth.FirebaseAuth
import com.yarolegovich.slidingrootnav.SlidingRootNav
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder
import com.yarolegovich.slidingrootnav.callback.DragStateListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.menu_left_drawer.*

class MainActivity : AppCompatActivity(), FragmentListener {

    private var slidingRootNav: SlidingRootNav? = null
    private lateinit var adapter: NavigationRVAdapter

    override fun onFragmentSelected(position: Int) { }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bundle = intent.extras
        val email = bundle?.getString("email")
        val provider = bundle?.getString("provider")

        if (email != null && provider != null) {
            loadMenu(savedInstanceState)
            saveAccount(email, provider)
            setUp(email)
        }
    }

    override fun onStart() {
        super.onStart()

        closeContent.setOnClickListener {
            slidingRootNav!!.closeMenu()
        }

        slidingRootNav?.isMenuOpened
    }


    private fun loadMenu(savedInstanceState: Bundle?) {
        slidingRootNav = SlidingRootNavBuilder(this)
            .withToolbarMenuToggle(toolbar)
            .withMenuOpened(false)
            .withContentClickableWhenMenuOpened(false)
            .withSavedState(savedInstanceState)
            .withMenuLayout(R.layout.menu_left_drawer)
            .withDragDistance(100)
            .addDragListener { object : DragStateListener{
                override fun onDragEnd(isMenuOpened: Boolean) {
                    if (isMenuOpened){
                        closeContent.visibility = View.VISIBLE
                    }else{
                        closeContent.visibility = View.GONE
                    }
                }

                override fun onDragStart() { }

            }}
            .inject()

        list.layoutManager = LinearLayoutManager(applicationContext)
        list.setHasFixedSize(true)
        updateAdapter(0)
        onNavigated()
        loadFrgment(Shipping())
    }

    private fun setUp(email: String) {
        correo.text = email
        closeSe.setOnClickListener {
            val pref = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
            pref.clear()
            pref.apply()

            FirebaseAuth.getInstance().signOut()
            onBackPressed()
        }
    }

    private fun saveAccount(email: String, provider: String){
        val pref = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
        pref.putString("email", email)
        pref.putString("provider", provider)
        pref.apply()
    }

    private fun loadFrgment(fragment: Fragment){
        supportFragmentManager.beginTransaction().replace(R.id.Container, fragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        slidingRootNav!!.closeMenu()
    }

    private fun loadItems() : List<Item>{
        val list = mutableListOf<Item>()

        list.add(
            Item(
                isSelected = true, name = "Envíos", icon = ContextCompat
                    .getDrawable(
                        applicationContext,
                        R.drawable.search_icon
                    )!!
            )
        )
        list.add(
            Item(
                isSelected = false, name = "Nuevo", icon = ContextCompat
                    .getDrawable(
                        applicationContext,
                        R.drawable.send_icon
                    )!!
            )
        )
        list.add(
            Item(
                isSelected = false, name = "Entregados", icon = ContextCompat
                    .getDrawable(
                        applicationContext,
                        R.drawable.package_icon
                    )!!
            )
        )
        list.add(
            Item(
                isSelected = false, name = "Tickets", icon = ContextCompat
                    .getDrawable(
                        applicationContext,
                        R.drawable.ic_note
                    )!!
            )
        )

        return list
    }

    private fun onNavigated(){
        list.addOnItemTouchListener(
            RecyclerTouchListener(this,
                object : ClickListener {
                    override fun onClick(view: View, position: Int) {
                        when (position) {
                            0 -> {
                                loadFrgment(Shipping())
                            }
                            1 -> {
                                loadFrgment(AddShip())
                            }
                            2 -> {
                                loadFrgment(Finished())
                            }
                            3 -> {
                                loadFrgment(TicketsList())
                            }
                        }

                        updateAdapter(position)

                        Handler(Looper.getMainLooper()).postDelayed({
                            slidingRootNav!!.closeMenu()}, 200
                        )
                    }
                }
            )
        )
    }

    private fun updateAdapter(highlightItemPos: Int) {
        adapter = NavigationRVAdapter(
            loadItems(),
            highlightItemPos
        )
        list.adapter = adapter
        adapter.notifyDataSetChanged()
    }
}

enum class ProviderType{
    EMAIL,
    GOOGLE
}