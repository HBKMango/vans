package app.android.vansallende

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Toast
import app.android.vansallende.model.NewShip
import app.android.vansallende.presenters.PresenterUpdate
import app.android.vansallende.presenters.contracts.ContractUpdate
import app.android.vansallende.ui.util.States.collected
import app.android.vansallende.ui.util.States.delivered
import app.android.vansallende.ui.util.States.failed
import app.android.vansallende.ui.util.States.redeliveryProcess
import app.android.vansallende.ui.util.States.shippingProcess
import com.google.zxing.integration.android.IntentIntegrator
import com.guardanis.sigcap.SignatureEventListener
import com.guardanis.sigcap.SignatureResponse
import com.guardanis.sigcap.dialog.SignatureDialogBuilder
import com.guardanis.sigcap.exceptions.CanceledSignatureInputException
import com.guardanis.sigcap.exceptions.NoSignatureException
import kotlinx.android.synthetic.main.activity_update.*

class UpdateActivity : AppCompatActivity(), ContractUpdate.View{

    var selected = 0
    var bitmap1: Bitmap? = null

    lateinit var presenter: PresenterUpdate
    private val options = arrayOf(collected, shippingProcess, delivered, failed, redeliveryProcess)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update)
        presenter = PresenterUpdate(this, this)
        spinner.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, options)
    }

    override fun onStart() {
        super.onStart()

        toDraw(firm)

        Scan.setOnClickListener {
            IntentIntegrator(this).setOrientationLocked(false).initiateScan()
        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selected = position
                if (position == 2){
                    ContentSig.visibility = View.VISIBLE
                }else {
                    ContentSig.visibility = View.GONE
                }
            }
        }

        Save.setOnClickListener {
            if (selected == 2){
                if (bitmap1 != null){
                    presenter.finishShip(options[selected], bitmap1!!)
                }
                else{
                    Toast.makeText(this, "Ingrese la firma para poder actualizar", Toast.LENGTH_LONG).show()
                }
            } else{
                presenter.updateShip(options[selected])
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (result != null){
            if (result.contents != null){
                presenter.browseShip(result.contents)
            }else{
                Toast.makeText(applicationContext, "Error al leer la información", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onFound(data: NewShip?) {
        if (data != null) {
            sName.text = data.transmitter!!.name
            sAddress.text = data.transmitter!!.address
            sPhone.text = data.transmitter!!.phone
            sContent.text = data.transmitter!!.description

            rName.text = data.receiver!!.name
            rPhone.text = data.receiver!!.phone
            rStreet.text = data.receiver!!.street
            rNo.text = data.receiver!!.no
            rCol.text = data.receiver!!.suburb
            rMun.text = data.receiver!!.municipality
            rCP.text = data.receiver!!.cp
            rState.text = data.receiver!!.state

            if (data.state == delivered){
                txtState.visibility = View.VISIBLE
                txtState.text = data.state
                spinner.visibility = View.GONE
            }
            else {
                txtState.visibility = View.GONE
                spinner.visibility = View.VISIBLE
                options.withIndex().forEach {
                    if (data.state == it.value){
                        spinner.setSelection(it.index)
                        selected = it.index
                    }
                }
            }
        }
    }

    override fun onUpdated() {
        Toast.makeText(applicationContext, "Estado actualizado", Toast.LENGTH_LONG).show()
    }

    override fun onFinished() {
        Toast.makeText(applicationContext, "Paquete entregado", Toast.LENGTH_LONG).show()
    }

    override fun onResponseFailure(throwable: String) {
        Toast.makeText(applicationContext, "Error a obtener datos", Toast.LENGTH_SHORT).show()
    }

    override fun showLottie() {}

    override fun hideLottie() {}

    private fun toDraw(imageView: ImageView){
        val eventListener: SignatureEventListener = object : SignatureEventListener {
            override fun onSignatureInputError(e: Throwable) {
                when (e) {
                    is NoSignatureException -> {
                        Toast.makeText(this@UpdateActivity, "Firma vacia", Toast.LENGTH_LONG).show()
                    }
                    is CanceledSignatureInputException -> {
                        // They clicked cancel
                        Toast.makeText(this@UpdateActivity, "Firma cancelada", Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        Toast.makeText(this@UpdateActivity, "Signature error", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onSignatureEntered(response: SignatureResponse) {
                val signatureImage = response.result
                bitmap1 = signatureImage
                imageView.setImageBitmap(signatureImage)
            }
        }

        imageView.setOnClickListener {
            SignatureDialogBuilder().showStatelessAlertDialog(this, eventListener)
        }
    }
}