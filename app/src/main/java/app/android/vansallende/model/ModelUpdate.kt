package app.android.vansallende.model

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.widget.Toast
import app.android.vansallende.presenters.contracts.ContractUpdate
import app.android.vansallende.ui.util.FileUploadListener
import app.android.vansallende.ui.util.ModelUtils.toShip
import app.android.vansallende.ui.util.Uploader
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.lang.Exception
import kotlin.math.roundToInt

class ModelUpdate(val context: Context) : ContractUpdate.Model{
    var id = ""

    override fun browseShip(
        onFinishedListener: ContractUpdate.Model.OnFinishedListener, data: String) {
        id = data

        FireBaseConnection.initCollectionReference().document(data).get().addOnSuccessListener {
            try {
                onFinishedListener.onFound(it?.toShip().apply { this?.id = it.id })
            }
            catch (e : Exception){
                onFinishedListener.onFailure("Error al mostrar los datos")
            }
        }
            .addOnFailureListener {
                onFinishedListener.onFailure(it.message!!)
            }
    }

    override fun updateShip(onFinishedListener: ContractUpdate.Model.OnFinishedListener, data: String) {
        val map = HashMap<String, Any>()
        map["state"] = data

        FireBaseConnection.initCollectionReference().document(id).update(map).addOnFailureListener {
            onFinishedListener.onFailure("Error al actualizar datos")
        }.addOnSuccessListener {
            onFinishedListener.onUpdated()
        }
    }

    override fun finishShip(
        onFinishedListener: ContractUpdate.Model.OnFinishedListener, data: String, bitmap: Bitmap) {

        Uploader.uploadSignature(save(bitmap), object: FileUploadListener {
            override fun onFileUploaded(url: String) {

                val map = HashMap<String, Any>()
                map["state"] = data
                map["signature"] = url

                FireBaseConnection.initCollectionReference().document(id).update(map).addOnFailureListener {
                    onFinishedListener.onFailure("Error al actualizar datos")
                }.addOnSuccessListener {
                    onFinishedListener.onFinished()
                }
            }

            override fun onFileUploadError(error: String?) {
                onFinishedListener.onFailure(error!!)
            }
        })
    }

    private fun save(bitmap: Bitmap) : Uri {
        val destPath: String = context.getExternalFilesDir(null)!!.absolutePath//+File.separator + "Vans"
        val file = File(destPath, "${System.currentTimeMillis()}.jpg")

        try {
            val stream: OutputStream = FileOutputStream(file)
            val scale : Double = bitmap.width.toDouble()/ bitmap.height.toDouble()
            val newHeight = 800/scale
            val imageScaled = Bitmap.createScaledBitmap(bitmap, 800, newHeight.roundToInt(), false)

            imageScaled.compress(Bitmap.CompressFormat.JPEG, 80, stream)

            stream.flush()
            stream.close()
        } catch (e: IOException){
            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
        }
        return Uri.parse(file.absolutePath)
    }

}