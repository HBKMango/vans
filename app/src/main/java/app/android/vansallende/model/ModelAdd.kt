package app.android.vansallende.model

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.widget.Toast
import app.android.vansallende.presenters.contracts.ContractAdd
import app.android.vansallende.ui.util.FileUploadListener
import app.android.vansallende.ui.util.Uploader
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import kotlin.math.roundToInt

class ModelAdd(val context : Context) : ContractAdd.Model{

    override fun addShip(onFinishedListener: ContractAdd.Model.OnFinishedListener, data: NewShip) {

        val a = data
        var bit1: String?

        FireBaseConnection.initCollectionReference().add(data)
            .addOnSuccessListener {
                onFinishedListener.onShipAdded(it.id)
            }
            .addOnFailureListener {
                onFinishedListener.onFailure(it.message.toString())
            }

        /*Uploader.uploadSignature(save(s1), object: FileUploadListener {
            override fun onFileUploaded(url: String) {

                bit1 = url

                Uploader.uploadSignature(save(s2), object: FileUploadListener {
                    override fun onFileUploaded(url: String) {

                        val newData = data.apply {
                            senderSignature = url
                            receiverSignature = bit1
                        }

                        connect.initCollectionReference().add(newData)
                            .addOnSuccessListener {
                                onFinishedListener.onShipAdded(it.id)
                            }
                            .addOnFailureListener {
                                onFinishedListener.onFailure(it.message.toString())
                            }
                    }

                    override fun onFileUploadError(error: String?) {
                        onFinishedListener.onFailure(error!!)
                    }
                })
            }

            override fun onFileUploadError(error: String?) {
                onFinishedListener.onFailure(error!!)
            }
        })*/
    }

    private fun save(bitmap: Bitmap) : Uri{
        val destPath: String = context.getExternalFilesDir(null)!!.absolutePath//+File.separator + "Vans"
        val file = File(destPath, "${System.currentTimeMillis()}.jpg")

        try {
            val stream: OutputStream = FileOutputStream(file)
            val scale : Double = bitmap.width.toDouble()/ bitmap.height.toDouble()
            val newHeight = 800/scale
            val imageScaled = Bitmap.createScaledBitmap(bitmap, 800, newHeight.roundToInt(), false)

            imageScaled.compress(Bitmap.CompressFormat.JPEG, 80, stream)

            stream.flush()
            stream.close()
        } catch (e: IOException){
            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
        }
        return Uri.parse(file.absolutePath)
    }
}