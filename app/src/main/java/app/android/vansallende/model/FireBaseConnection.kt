package app.android.vansallende.model

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage

object FireBaseConnection {

    fun initStorage() : FirebaseStorage{
        return FirebaseStorage.getInstance()
    }

    private fun initFireStore() : FirebaseFirestore{
        return FirebaseFirestore.getInstance()
    }

    fun initCollectionReference() : CollectionReference{
        return initFireStore().collection("send")
    }

}