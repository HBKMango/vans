package app.android.vansallende.model

import android.content.Context
import app.android.vansallende.presenters.contracts.ContractPreview
import app.android.vansallende.ui.util.ModelUtils.toShip
import com.google.firebase.firestore.DocumentSnapshot
import java.lang.Exception

class ModelPreview(val context : Context) : ContractPreview.Model {

    override fun browseShip(
        onFinishedListener: ContractPreview.Model.OnFinishedListener, data: String) {

        FireBaseConnection.initCollectionReference().document(data).get().addOnSuccessListener {
            try {
                onFinishedListener.onFound(it?.toShip().apply { this?.id = it.id })
            }
            catch (e : Exception){
                onFinishedListener.onFailure("Error al mostrar los datos")
            }
        }
            .addOnFailureListener {
                onFinishedListener.onFailure(it.message!!)
            }
    }
}