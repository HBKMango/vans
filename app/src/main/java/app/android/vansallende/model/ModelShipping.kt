package app.android.vansallende.model

import app.android.vansallende.presenters.contracts.ContractShipping
import app.android.vansallende.ui.util.ModelUtils.toShip
import app.android.vansallende.ui.util.States.delivered
import com.google.firebase.firestore.Query

class ModelShipping : ContractShipping.Model {

    override fun getShipping(onFinishedListener: ContractShipping.Model.OnFinishedListener) {
        FireBaseConnection.initCollectionReference()
            .orderBy("date", Query.Direction.DESCENDING)
            .addSnapshotListener{ snapshot, ex ->

                if (ex != null){
                    onFinishedListener.onFailure(ex.message!!)
                } else {
                    val list : MutableList<NewShip> = mutableListOf()
                    snapshot!!.documents.forEach{
                        list.add(it.toShip()!!.apply { this.id = it.id })
                    }

                    val newList = list.filter { it.state != delivered}

                    onFinishedListener.onShippingGotten(newList)
                }
            }
    }
}