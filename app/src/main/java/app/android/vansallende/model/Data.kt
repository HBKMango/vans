package app.android.vansallende.model

import android.graphics.drawable.Drawable

data class Item(
    var isSelected : Boolean,
    val name : String,
    val icon : Drawable
)

data class NewShip(
    var id : String = "0",
    var transmitter : Transmitter?,
    var receiver : Receiver?,
    var content : Content?,
    var date : String,
    var signature : String?,
    var state : String
){
    constructor() : this(
        "",
        Transmitter("Error", "", "",""),
        Receiver("", "", "", "","","", "", ""),
        Content(0,0,0,0,0),
        date = "Error",
        signature = "",
        state = ""
    )
}

data class Content(
    val small : Int,
    val middle : Int,
    val big : Int,
    val xBig : Int,
    val price : Int
){
    constructor() : this(0,0,0,0,0)
}

data class Receiver(
    val name: String,
    val phone: String,
    val street : String,
    val no : String,
    val suburb : String,
    val municipality: String,
    val cp: String,
    val state: String
){
    constructor() : this("","","","","","","","")
}

data class Transmitter(
    val name: String,
    val phone: String,
    val address: String,
    val description: String
){
    constructor() : this("","","","")
}